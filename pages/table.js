import Layout from '../components/Layout';
import BlogTable from '../components/content/BlogTable';

const Table = () => {
    return (
        <Layout>
            <h1 className="lg:text-2xl text-lg mb-6">All Posts</h1>
            <div className="bg-white lg:px-6 px-4 lg:py-4 py-2 shadow-sm">
                <h3 className="lg:text-lg text-sm">List of Posts</h3>
                <small className="text-gray-600">
                    You can search and sort items
                </small>
                <BlogTable />
            </div>
        </Layout>
    );
}
 
export default Table;