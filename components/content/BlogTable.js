import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const BlogTable = () => {
    return (
        <>
            <div className="relative flex lg:w-64 w-full flex-wrap items-stretch mb-3 mt-4">
                <span className="z-10 h-full leading-snug font-normal absolute text-center text-slate-300 absolute bg-transparent rounded text-base items-center justify-center w-8 pl-3 py-2">
                    <FontAwesomeIcon
                        icon={faSearch}
                    />
                </span>
                <input
                    type="text"
                    placeholder="Search"
                    className="px-3 py-2 placeholder-slate-300 text-slate-600 relative bg-white rounded text-sm border-0 shadow outline-none focus:outline-none focus:ring focus:ring-gray-200 w-full pl-10"
                />
            </div>
            <div className="overflow-hidden">
                <table className="w-full text-left table-fixed mb-4">
                    <thead className="bg-gray-100 text-gray-800">
                        <tr className="lg:text-lg text-sm border">
                            <th className="lg:px-4 px-2 lg:py-4 py-2 border">No.</th>
                            <th className="lg:px-4 px-2 lg:py-4 py-2 border">Title</th>
                            <th className="lg:px-4 px-2 lg:py-4 py-2 border">Artist</th>
                            <th className="lg:px-4 px-2 lg:py-4 py-2 border">Action</th>
                        </tr>
                    </thead>
                    <tbody className="bg-white text-gray-800">
                        <tr className="lg:text-lg text-sm">
                            <td className="lg:px-4 px-2 lg:py-2 py-1 border">1</td>
                            <td className="lg:px-4 px-2 lg:py-2 py-1 border">Kita</td>
                            <td className="lg:px-4 px-2 lg:py-2 py-1 border">Sheila On 7</td>
                            <td className="lg:px-4 px-2 lg:py-2 py-1 border">
                                <button
                                    type="button"
                                    className="uppercase py-1 px-2 text-blue-500 text-sm rounded-md bg-blue-100 border border-blue-100 hover:bg-blue-500 hover:text-white hover:border-blue-500 focus:outline-none shadow font-medium"
                                >
                                    View
                                </button>
                            </td>
                        </tr>
                        <tr className="lg:text-lg text-sm">
                            <td className="lg:px-4 px-2 lg:py-2 py-1 border">2</td>
                            <td className="lg:px-4 px-2 lg:py-2 py-1 border">Dan</td>
                            <td className="lg:px-4 px-2 lg:py-2 py-1 border">Sheila On 7</td>
                            <td className="lg:px-4 px-2 lg:py-2 py-1 border">
                                <button
                                    type="button"
                                    className="uppercase py-1 px-2 text-blue-500 text-sm rounded-md bg-blue-100 border border-blue-100 hover:bg-blue-500 hover:text-white hover:border-blue-500 focus:outline-none shadow font-medium"
                                >
                                    <span>

                                    </span>
                                    View
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    );
}
 
export default BlogTable;