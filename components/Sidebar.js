import { faBars, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

const Sidebar = () => {
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [dropdown, setDropdown] = useState(false);
    const router = useRouter();

    return (
        <>
            <div className="lg:flex flex-col lg:flex-row lg:min-h-screen w-full">
                <div className="flex flex-col w-full lg:w-64 text-gray-700 bg-white dark-mode:text-gray-200 dark-mode:bg-gray-800 flex-shrink-0">
                    <div className="flex-shrink-0 lg:px-8 px-4 lg:py-4 py-2 flex flex-row items-center justify-between">
                        <Link
                            href="/admin"
                        >
                            <a
                                className="text-lg lg:text-2xl font-semibold tracking-widest text-gray-900 uppercase rounded-lg dark-mode:text-white focus:outline-none focus:shadow-outline"
                            >
                                Anggit Dashboard
                            </a>
                        </Link>
                        <button
                            className={
                                "rounded-lg lg:hidden rounded-lg focus:outline-none focus:shadow-outline"
                            }
                            onClick={() => setSidebarOpen(!sidebarOpen)}
                        >
                            {
                                <FontAwesomeIcon
                                    icon={faBars}
                                    className={
                                        "transition-all" +
                                        (sidebarOpen ? ' rotate-90' : ' rotate-0')
                                    }
                                />
                            }
                        </button>
                    </div>
                    <nav
                        className={
                            "flex-grow lg:block px-4 pb-4 lg:pb-0 lg:overflow-y-auto" +
                            (sidebarOpen ? ' block' : ' hidden')
                        }
                    >
                        <Link
                            href="/"
                        >
                            <a
                                className={
                                    "block px-4 py-2 mt-2 text-sm lg:text-lg font-semibold text-gray-900 rounded-lg dark-mode:hover:bg-gray-600 dark-mode:focus:bg-gray-600 dark-mode:focus:text-white dark-mode:hover:text-white dark-mode:text-gray-200 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" +
                                    (router.pathname == '/admin' ? ' bg-gray-200 dark-mode:bg-gray-700' : '')
                                }
                            >
                                Home
                            </a>
                        </Link>
                        <Link
                            href="/table"
                        >
                            <a
                                className={
                                    "block px-4 py-2 mt-2 text-sm lg:text-lg font-semibold text-gray-900 rounded-lg dark-mode:hover:bg-gray-600 dark-mode:focus:bg-gray-600 dark-mode:focus:text-white dark-mode:hover:text-white dark-mode:text-gray-200 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" +
                                    (router.pathname == '/table' ? ' bg-gray-200 dark-mode:bg-gray-700' : '')
                                }
                            >
                                Table
                            </a>
                        </Link>
                        <div className="relative">
                            <button
                                className="flex flex-row items-center w-full px-4 py-2 mt-2 text-sm lg:text-lg font-semibold text-left bg-transparent rounded-lg dark-mode:bg-transparent dark-mode:focus:text-white dark-mode:hover:text-white dark-mode:focus:bg-gray-600 dark-mode:hover:bg-gray-600 lg:block hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline"
                                onClick={() => setDropdown(!dropdown)}
                            >
                                <span>Dropdown</span>
                                <FontAwesomeIcon
                                    icon={faChevronDown}
                                    className={
                                        "inline w-4 h-4 mt-1 ml-1 transition-transform duration-200 transform lg:-mt-1" +
                                        (!dropdown ? ' rotate-0' : ' rotate-180')
                                    }
                                />
                            </button>
                            <div
                                className={
                                    "absolute right-0 w-full mt-2 origin-top-right rounded-md shadow-lg" +
                                    (!dropdown ? ' hidden' : '')
                                }
                            >
                                <div className="px-2 py-2 bg-white rounded-md shadow dark-mode:bg-gray-800">
                                    <a className="block px-4 py-2 mt-2 text-sm lg:text-lg font-semibold bg-transparent rounded-lg dark-mode:bg-transparent dark-mode:hover:bg-gray-600 dark-mode:focus:bg-gray-600 dark-mode:focus:text-white dark-mode:hover:text-white dark-mode:text-gray-200 lg:mt-0 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" href="#">Link #1</a>
                                    <a className="block px-4 py-2 mt-2 text-sm lg:text-lg font-semibold bg-transparent rounded-lg dark-mode:bg-transparent dark-mode:hover:bg-gray-600 dark-mode:focus:bg-gray-600 dark-mode:focus:text-white dark-mode:hover:text-white dark-mode:text-gray-200 lg:mt-0 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" href="#">Link #2</a>
                                    <a className="block px-4 py-2 mt-2 text-sm lg:text-lg font-semibold bg-transparent rounded-lg dark-mode:bg-transparent dark-mode:hover:bg-gray-600 dark-mode:focus:bg-gray-600 dark-mode:focus:text-white dark-mode:hover:text-white dark-mode:text-gray-200 lg:mt-0 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" href="#">Link #3</a>
                                </div>
                            </div>
                        </div>
                        <Link
                            href="/logout"
                            activeClassName="bg-gray-200 dark-mode:bg-gray-700"
                        >
                            <a
                                className="block px-4 py-2 mt-2 text-sm lg:text-lg font-semibold text-gray-900 rounded-lg dark-mode:hover:bg-gray-600 dark-mode:focus:bg-gray-600 dark-mode:focus:text-white dark-mode:hover:text-white dark-mode:text-gray-200 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline"
                            >
                                Logout
                            </a>
                        </Link>
                    </nav>
                </div>
            </div>
        </>
    );
}
 
export default Sidebar;