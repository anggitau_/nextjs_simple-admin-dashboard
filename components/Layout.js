import Sidebar from "./Sidebar";

const Layout = ({ children }) => {
    return (
        <>
            <div className="flex lg:flex-row flex-col">
                <aside className="w-full lg:w-64 lg:fixed lg:top-0 lg:left-0">
                    <Sidebar />
                </aside>
                <main className="lg:w-full lg:ml-64">
                    <div className="lg:min-h-screen bg-gray-50 lg:p-10 p-4">
                        {children}
                    </div>
                </main>
            </div>
        </>
    );
}
 
export default Layout;