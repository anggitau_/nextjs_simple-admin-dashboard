This is a simple admin dashboard build using [Next.js](https://nextjs.org/) & [Tailwind CSS](https://tailwindcss.com).

## Getting Started

First, run:

```bash
npm install
# or
yarn install
```

Second, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial

To learn more about Tailwind, take a look at the following resources:

- [Tailwind CSS Documentation](https://tailwindcss.com/docs/installation) - learn about Tailwind CSS.